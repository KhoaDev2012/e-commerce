"use client";

import { useState, useEffect } from "react";

import { StoreModal } from "@/components/modals/storeModal";

export const ModalProvider = () => {
  // execute hydrate error between ClientComponent and ServerComponent
  // cuz in client component will trigger a lots and server component will not
  // so this lifecycle below ensure execute hydrate err
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, []);

  if (!isMounted) {
    return null;
  }

  return (
    <>
      <StoreModal />
    </>
  );
};
