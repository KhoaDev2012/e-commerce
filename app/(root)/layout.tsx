import prismadb from "@/lib/prismadb";
import { auth } from "@clerk/nextjs/server";
import { redirect } from "next/navigation";

// this layout used to check store create or not
// if create store will redirect to dashboard
// if not will keep user stay in modal create store

export default async function SetupLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const { userId } = auth();

  if (!userId) {
    redirect("/sign-in");
  }

  const store = await prismadb.store.findFirst({
    where: {
      userId,
    },
  });

  // check store exist redirect to layout dashboard where will confirm again about userId and storeId
  if (store) {
    redirect(`/${store.id}`);
  }

  return <>{children}</>;
}
