"use client";

import { useEffect } from "react";

import { useStoreModal } from "@/hooks/useStoreModal";

// this page will trigger the modal cuz this modal will appear when user login

const SetupPage = () => {
  // lấy state để trigger ở mỗi modal nào mình muốn
  // k cần return về Modal Component rồi import các props dài dòng vào
  const onOpen = useStoreModal((state) => state.onOpen);
  const isOpen = useStoreModal((state) => state.isOpen);

  // trigger to always open modal create store
  // if have a store also can close it
  useEffect(() => {
    if (!isOpen) {
      onOpen();
    }
  }, [isOpen, onOpen]);

  return <div className="p-4">Root Page</div>;
};

export default SetupPage;
